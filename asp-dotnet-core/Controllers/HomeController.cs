﻿using Microsoft.AspNetCore.Mvc;

namespace asp_dotnet_core_demo.Controllers
{
    public class HomeController : ControllerBase
    {
        public ObjectResult Index()
        {
            return StatusCode(200, "Hello World ASP.NET CORE Demo@ABA");
        }
    }
}
