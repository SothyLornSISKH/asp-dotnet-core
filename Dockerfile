FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app
ENV PROJ_FOLDER="asp-dotnet-core" \
    PROJ_NAME="asp-dotnet-core-demo"

COPY $PROJ_FOLDER/$PROJ_NAME.csproj ./
RUN dotnet restore

# Copy everything else and build

COPY . ./
RUN dotnet publish $PROJ_FOLDER/$PROJ_NAME.csproj -c Release  -o deploy
# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
# COPY ababank_com.pfx .
COPY --from=build-env /app/deploy .
ENV ASPNETCORE_URLS=http://*:5000
ENV ASPNETCORE_ENVIRONMENT=Production
ENTRYPOINT ["dotnet", "asp-dotnet-core-demo.dll"]
